// Needs a rename

var beginningGameModule = function() {
	var gameModule = angular.module('game');
	var beginningModule = gameModule.controller('beginningModuleController', ['$scope', 'storyService', 'stash', function ($scope, storyService, stash) {
		$scope.Name = 'Beginning Module here';
		$scope.Light = function() {
			if (stash.RequestResources({Newspaper: 1})) {
				storyService.AddLine('The fire has been lit.');
			}
			else {
				storyService.AddLine('not enough newspaper to get the fire going');
			}
		}
	}]);
}();