var storyService = function() {
	var services = angular.module('services').factory('storyService', [function () {
		var uniqueMessageID = 0;
		var log = [];

		var getLog = function() {
			return log;
		};

		var addLine = function(line) {
			var newEntry = {};
			newEntry.content = line;
			newEntry.ID = uniqueMessageID++;
			log.push(newEntry);
		};

		return {
			GetLog : getLog,
			AddLine : addLine,
			Log : log
		};
	}]);
}();