var stashService = function() {
	var services = angular.module('services');


	services.factory('stash', ['$http', function ($http) {

		var resources = {};

		function loadResources() {
			//Newspaper
			$http.get('../Objects/Resources/Newspaper.json')
			.success(function (data, status, headers, config){
				data.amount = 5;
				resources.Newspaper = data;
			})
			.error(function (data, status, headers, config){
			});
			resources.TestPlastic = {
				amount:2,
				name: "testplastic",
				description: "Useful for insulation or to burn for heat"
				};
		}

		function checkResources(requestedResources) {
			var result = true ;
			for(var key in requestedResources){
				if (result && requestedResources.hasOwnProperty(key) && resources.hasOwnProperty(key)) {
					result = result && requestedResources[key] <= resources[key].amount;
				}
				else {
					result = false;
				}
			}
			return result ;
		}

		function spendResources(requestedResources) {
			for(var key in requestedResources) {
				resources[key].amount -= requestedResources[key];
				//Just round to zero
				if(resources[key].amount < 0) {
					resources[key].amount = 0;
				}

			}
		}

		function requestResources(requestResources) {
			var result = true;
			if (checkResources(requestResources)) {
				spendResources(requestResources);
			}
			else {
				result = false;
			}
			return result;
		}

		loadResources();

		return {
			Resources: resources,
			RequestResources : requestResources
		};
	}]);
}();
