var resourceDisplay = function()
{
	var garbageModule = angular.module('garbage');
	garbageModule.controller('resourceDisplayController', ['$scope', 'stash', function ($scope, stash){
		$scope.Name = 'The stash';
		$scope.Resources = stash.Resources;
		$scope.Newspaper = stash.Newspaper;
	}])
}();