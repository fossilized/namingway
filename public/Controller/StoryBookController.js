var storyBook = function() {
	var garbageModule = angular.module('garbage');
	var storyController = garbageModule.controller('storyBookController', ['$scope', 'storyService', function ($scope, storyService) {
		$scope.Name = 'The story so far...';

		$scope.Log = storyService.Log;
	}]);
}();