var actionDisplay = angular.module('directives', [function() {
    return {
      scope : {
        actions:'=moduleActions'
      },
      templateUrl: './directives/Engine/ActionDisplay.html',
      restrict: 'E'
    };
}]);
