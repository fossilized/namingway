var gameModules = angular.module('directives').directive('gameModules', [function() {
  return {
    templateUrl: './directives/Engine/GameModules.html',
    restrict: 'E'
  }
}]);
