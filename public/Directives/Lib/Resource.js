var resourceDirective = angular.module('directives').directive('resource', [function() {
  return {
    scope: {
      resourceModel: '='
    },
    templateUrl: './directives/Lib/Resource.html',
    restrict: 'AEC'
  }
}]);
