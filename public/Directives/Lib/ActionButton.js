var actionDirective = angular.module('directives').directive('actionButton', [function () {
	return {
		scope : {
			actionComponent: '=action'
		},
		templateUrl: './directives/Lib/ActionButton.html',
		restrict: 'AEC'
	};
}]);
