describe("Unit: Services:", function() {
	describe("Story", function() {

		beforeEach(module('services'));

		it('contains a story service', inject(
			function(storyService) {
				expect(storyService).not.toBe(null);
			}));
	});
});

